# Roles
> Remember principle of least priviledge. Most roles here have too broad permissions for production and some should be separated using different SAs (e.g, Pub/Sub Publisher, Dataflow Admin)

* Pub/Sub Subscriber
    * Read from Pub/Sub topic
* BigQuery Data Editor
    * Manage BQ dataset
* Storage Admin
    * Manage GCS buckets and objects
* Service Account User
    * Use other SAs
* Dataflow Admin
    * Create Dataflow jobs
* Pub/Sub Publisher
    * Write to Pub/Sub topic