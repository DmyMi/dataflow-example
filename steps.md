# Steps
1. Create a PubSub topic and subscription
2. Create a BigQuery dataset and table (schema: `timestamp:TIMESTAMP,temperature:FLOAT,msg:STRING`)
3. (Optional) Create a Cloud Storage Bucket
4. Create a service account (see **sa_roles.md**)
5. Write code
6. Run pipeline
7. Profit