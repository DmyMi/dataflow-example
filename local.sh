#!/usr/bin/env bash

export GOOGLE_APPLICATION_CREDENTIALS=$HOME/key.json
python job.py \
--runner DirectRunner \
--streaming \
--input_subscription projects/PROJECT_ID/subscriptions/SUBSCRIPTION \
--output_table PROJECT_ID:DATASET_NAME.TABLE_NAME \
--output_schema "timestamp:TIMESTAMP,temperature:FLOAT,msg:STRING"