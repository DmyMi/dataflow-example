#!/usr/bin/env python

import json
import time
from datetime import datetime
from random import random
from google.cloud import pubsub_v1

PROJECT_ID = "PROJECT_ID"
TOPIC_ID = "TOPIC_NAME"
MAX_MESSAGES = 10000

# --- PubSub Utils Class
class PubSubPublisher:
    def __init__(self, project_id, topic_id):
        self.project_id = project_id
        self.topic_id = topic_id
        self.publisher = pubsub_v1.PublisherClient()
        self.topic_path = self.publisher.topic_path(self.project_id, self.topic_id)
        self.publish_futures = []

    def publish(self, data: str):
        result = self.publisher.publish(self.topic_path, data.encode("utf-8"))
        return result

# --- Main publishing script
def main():
    i = 0
    publisher = PubSubPublisher(PROJECT_ID, TOPIC_ID)
    while i < MAX_MESSAGES:
        temp = random() * 100
        data = {
            "timestamp": int(datetime.utcnow().timestamp()),
            "temperature": temp,
            "msg": "High!!!" if temp > 30 else "Ok"
        }
        publisher.publish(json.dumps(data))
        time.sleep(random())
        i += 1

if __name__ == "__main__":
    main()