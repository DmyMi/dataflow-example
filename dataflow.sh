#!/usr/bin/env bash

export GOOGLE_APPLICATION_CREDENTIALS=$HOME/key.json

python job.py \
--streaming \
--runner DataflowRunner \
--project PROJECT_ID \
--region europe-west3 \
--temp_location gs://TEMP_BUCKET/ \
--job_name dataflow-pipeline-v1 \
--max_num_workers 2