#!/usr/bin/env python

import argparse
from datetime import datetime
import json
import logging

# Beam Imports
import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions, StandardOptions

# Defaults for my project, dataset & subscription
INPUT_SUBSCRIPTION = "projects/PROJECT_ID/subscriptions/SUBSCRIPTION"
BIGQUERY_TABLE = "PROJECT_ID:DATASET_NAME.TABLE_NAME"
BIGQUERY_SCHEMA = "timestamp:TIMESTAMP,temperature:FLOAT,msg:STRING"

class ParseFunction(beam.DoFn):
    """ Custom transformation to apply to each element in stream """

    def process(
        self,
        element: bytes, # We know that we're getting binary data from PubSub
        timestamp=beam.DoFn.TimestampParam, # binds the timestamp information as an object, ain't using it for this example
        window=beam.DoFn.WindowParam): # binds the window information as an object, ain't using it for this example
        """ Simple processing function """
        from datetime import datetime
        parsed = json.loads(element.decode("utf-8"))
        parsed["timestamp"] = datetime.utcfromtimestamp(parsed["timestamp"]).strftime('%Y-%m-%d %H:%M:%S')
        yield parsed

def run(input_subscription, output_table, output_schema, pipeline_args=None):

    # Creating pipeline options
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(StandardOptions).streaming = True

    # Defining our pipeline and its steps
    with beam.Pipeline(options=pipeline_options) as p:
        (
            p
            | "Read from Pub/Sub" >> beam.io.gcp.pubsub.ReadFromPubSub(
                subscription=input_subscription, timestamp_attribute=None
            )
            | "Parse Input" >> beam.ParDo(ParseFunction())
            | "Write to BigQuery" >> beam.io.WriteToBigQuery(
                output_table,
                schema=output_schema,
                write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND,
            )
        )


if __name__ == "__main__":
    # Full logging for demo
    logging.basicConfig(level=logging.INFO)
    logging.getLogger().setLevel(logging.INFO)

    # Parsing arguments
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input_subscription",
        help='Input PubSub subscription. Format: "projects/<PROJECT>/subscriptions/<SUBSCRIPTION>".',
        default=INPUT_SUBSCRIPTION,
    )

    parser.add_argument(
        "--output_table", help="Output BigQuery Table", default=BIGQUERY_TABLE
    )

    parser.add_argument(
        "--output_schema",
        help="Output BigQuery Schema in text format",
        default=BIGQUERY_SCHEMA,
    )

    known_args, pipeline_args = parser.parse_known_args()

    run(
        known_args.input_subscription,
        known_args.output_table,
        known_args.output_schema,
        pipeline_args
    )